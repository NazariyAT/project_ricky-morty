import { LoadingService } from './../../shared/service/local/loading.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  isLoading: boolean = false;
  isLoadingSubs$: any;

  constructor(private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.isLoadingSubs$ = this.loadingService.getIsLoading().subscribe((isLoading: any) => {
      this.isLoading = isLoading;
    });
  }
  ngOnDestroy(){
    this.isLoadingSubs$.unsubscribe();
  }

}
