import { LoadingService } from './../../shared/service/local/loading.service';
import { LocationsService } from './../../shared/service/locations.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locationList:any = [];
  info:any={}

  constructor(private locationsService:LocationsService, private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);
    this.locationsService.getLocations().subscribe(param=>{
      this.locationList = param.results;
      this.info = param.info;
      this.loadingService.setIsLoading(false);
    })
  }

  public nextPage(){
    if(this.info.next){
      this.loadingService.setIsLoading(true);
      this.locationsService.getLocationPage(this.info.next).subscribe((location)=>{
        this.locationList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }
  public previousPage(){
    if(this.info.prev){
      this.loadingService.setIsLoading(true);
      this.locationsService.getLocationPage(this.info.prev).subscribe((location)=>{
        this.locationList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }




}
