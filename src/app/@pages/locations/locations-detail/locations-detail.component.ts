import { LoadingService } from './../../../shared/service/local/loading.service';
import { LocationsService } from './../../../shared/service/locations.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-locations-detail',
  templateUrl: './locations-detail.component.html',
  styleUrls: ['./locations-detail.component.scss']
})
export class LocationsDetailComponent implements OnInit {

  locationDetail:any={}
  residentsLoc:any=[];

  constructor(private route:ActivatedRoute, private locationsService: LocationsService,private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);
    this.route.paramMap.subscribe((param)=>{
      let idLocation = param.get('id')
      this.locationsService.getLocationsById(idLocation).subscribe((param)=>{
        this.locationDetail = param;

        for (let i = 0; i < param.residents.length; i++) {
          const urlResident = param.residents[i];
          this.locationsService.getCharactersbyUrl(urlResident).subscribe((resident)=>{
            this.residentsLoc.push(resident)
            this.loadingService.setIsLoading(false);
          })
          
        }
      })
    })
  }
  
  public up(){
    window.scrollTo(0,0);
  }

}
