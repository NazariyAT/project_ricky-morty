import { LocationsComponent } from './locations.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationsDetailComponent } from './locations-detail/locations-detail.component';

const routes: Routes = [
  {
    path:'', component: LocationsComponent
  },
  {
    path:':id', component: LocationsDetailComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationsRoutingModule { }
