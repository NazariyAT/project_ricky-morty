import { LoadingService } from './../../shared/service/local/loading.service';
import { EpisodesService } from './../../shared/service/episodes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss'],
})
export class EpisodesComponent implements OnInit {
  episodeList: any = [];
  info:any=[];

  constructor(private episodesService: EpisodesService, private loadingService:LoadingService) {}

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);
    this.episodesService.getEpisodes().subscribe((param) => {
      this.episodeList = param.results;
      this.info = param.info
      this.loadingService.setIsLoading(false);
    });
  }
  public nextPage(){
    if(this.info.next){
      this.loadingService.setIsLoading(true);
      this.episodesService.getEpisodesPage(this.info.next).subscribe((location)=>{
        this.episodeList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }

  public previousPage(){
    if(this.info.prev){
      this.loadingService.setIsLoading(true);
      this.episodesService.getEpisodesPage(this.info.prev).subscribe((location)=>{
        this.episodeList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }
}
