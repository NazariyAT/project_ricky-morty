import { EpisodesComponent } from './episodes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EpisodesDetailComponent } from './episodes-detail/episodes-detail.component';

const routes: Routes = [
  {
    path:'', component: EpisodesComponent
  },
  {
    path:':id', component: EpisodesDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpisodesRoutingModule { }
