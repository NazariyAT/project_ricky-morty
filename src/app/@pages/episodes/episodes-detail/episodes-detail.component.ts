import { LoadingService } from './../../../shared/service/local/loading.service';
import { EpisodesService } from './../../../shared/service/episodes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-episodes-detail',
  templateUrl: './episodes-detail.component.html',
  styleUrls: ['./episodes-detail.component.scss']
})
export class EpisodesDetailComponent implements OnInit {

  episodeDetail:any={}
  shownChar:any=[]

  constructor(private route:ActivatedRoute,private episodesService:EpisodesService, private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);
    this.route.paramMap.subscribe((param)=>{
      let id = param.get('id')
      this.episodesService.getEpisodeById(id).subscribe((params)=>{
        this.episodeDetail = params;  

        for (let i = 0; i < params.characters.length; i++) {
          const urlResident = params.characters[i];
          this.episodesService.getCharactersbyUrl(urlResident).subscribe((char)=>{
            this.shownChar.push(char)
            this.loadingService.setIsLoading(false);
          })
        }
      })
    })
  }


  public up(){
    window.scrollTo(0,0);
  }
}
