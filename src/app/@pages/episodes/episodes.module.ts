import { EpisodesDetailComponent } from './episodes-detail/episodes-detail.component';
import { EpisodesGalleryComponent } from './episodes-gallery/episodes-gallery.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpisodesRoutingModule } from './episodes-routing.module';
import { EpisodesComponent } from './episodes.component';


@NgModule({
  declarations: [
    EpisodesComponent,
    EpisodesGalleryComponent,
    EpisodesDetailComponent
  ],
  imports: [
    CommonModule,
    EpisodesRoutingModule
  ]
})
export class EpisodesModule { }
