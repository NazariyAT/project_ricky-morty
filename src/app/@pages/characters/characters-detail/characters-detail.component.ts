import { LoadingService } from './../../../shared/service/local/loading.service';
import { CharactersService } from './../../../shared/service/characters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-characters-detail',
  templateUrl: './characters-detail.component.html',
  styleUrls: ['./characters-detail.component.scss']
})
export class CharactersDetailComponent implements OnInit {
  
  characterDetail:any={}
  
  constructor(private route:ActivatedRoute,private charactersService:CharactersService,private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);
    this.route.paramMap.subscribe((param)=>{
      this.charactersService.getCharactersById(param.get('id')).subscribe((params)=>{
        this.characterDetail = params;
        this.loadingService.setIsLoading(false);
      })
    })
  }

  statusChangeColor(){
    if(this.characterDetail.status === 'Dead'){
      return 'red';
    }else if(this.characterDetail.status === 'unknown'){
      return 'orange';
    }else{
      return 'rgb(0, 255, 42)';
    }
  }
}

