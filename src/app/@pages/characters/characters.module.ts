import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';
import { CharactersComponent } from './characters.component';
import { CharactersGalleryComponent } from './characters-gallery/characters-gallery.component';
import { CharactersDetailComponent } from './characters-detail/characters-detail.component';


@NgModule({
  declarations: [
    CharactersComponent,
    CharactersGalleryComponent,
    CharactersDetailComponent
  ],
  imports: [
    CommonModule,
    CharactersRoutingModule
  ]
})
export class CharactersModule { }
