import { LoadingService } from './../../shared/service/local/loading.service';
import { CharactersService } from './../../shared/service/characters.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit{
  characterList: any = [];
  info:any ={};

  constructor(private charactersService:CharactersService, private loadingService:LoadingService) { }

  ngOnInit(): void {
    this.loadingService.setIsLoading(true);

    this.charactersService.getCharacters().subscribe((param) => {
      this.characterList = param.results;
      this.info = param.info
      this.loadingService.setIsLoading(false);
    });
  }
  public nextPage(){
    console.log(this.info.next)
    if(this.info.next){
      this.loadingService.setIsLoading(true);
      this.charactersService.getCharacterPage(this.info.next).subscribe((location)=>{
        this.characterList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }

  public previousPage(){
    if(this.info.prev){
      this.loadingService.setIsLoading(true);
      this.charactersService.getCharacterPage(this.info.prev).subscribe((location)=>{
        this.characterList = location.results
        this.info = location.info
        this.loadingService.setIsLoading(false);
      });
    }
  }

  public up(){
    window.scrollTo(0,0);
  }
}
